// ==UserScript==
// @name         CerealOgameStats
// @description  Make alliance stats from ogame to post in forums. Original code belong to Elias Grande
// @namespace    https://github.com/Ouraios/
// @downloadURL  https://github.com/Ouraios/CerealOgameStats/raw/master/dist/releases/latest.user.js
// @updateURL    https://github.com/Ouraios/CerealOgameStats/raw/master/dist/releases/latest.meta.js
// @icon         https://github.com/Ouraios/CerealOgameStats/raw/master/dist/img/icon.png
// @version      3.1.2
// @include      *://*.ogame.*/game/index.php?*page=alliance*
// @include      *://*.ogame.gameforge.*/game/index.php?*page=alliance*
// ==/UserScript==
